FROM mcr.microsoft.com/dotnet/core/runtime:3.0-buster-slim AS base
WORKDIR /app
COPY . ./

ENTRYPOINT ["dotnet", "TestConsoleApp.dll"]